package DataStrucktur.Trees.Swap_Nodes;

import java.io.*;
import java.math.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

public class Solution {

    static class Node {


        public Node(int value) {
            this.data = value;
            this.root = null;
            this.left = null;
            this.right = null;
        }
        int data;
        Node root;
        Node left;
        Node right;

        Node[] add(int left_child, int right_child) {
            if (left_child != -1) {
                this.left = new Node(left_child);
                left.root = this;
            } else {
                this.left = null;
            }
            if (right_child != -1) {
                this.right = new Node(right_child);
                right.root = this;
            } else {
                this.right = null;
            }

            return new Node[]{this.left, this.right};
        }

        public String toString(){
            String res = "Tree with root " + this.data;
            Node node_left = this.left;
            Node node_right = this.right;
            boolean flag =  true;
            while (flag) {
                if (node_left != null) {
                    res += "\n Left child of " + this.data + " = " + node_left.data + " \t";
                    res +=  node_left.toString();
                }
                if (node_right != null) {
                    res += "\n Right child of " + this.data + " = " + node_right.data + " \t ";
                    res += node_right.toString();
                }
                flag = false;
            }

            return res;
        }

    }

    static void create_tree(Node node, int[] [] indexes, int index) {

        if (node == null){
            return;
        }
        if (index >= indexes.length) {
            return;
        }
        Node [] childes = node.add(indexes[index][0],indexes[index][1]);

        if (childes[0] != null) {
            create_tree(childes[0], indexes, index +2);
        }
        if (childes[1] != null) {
            create_tree(childes[1], indexes, index +3);
        }

    }



    static ArrayList<Integer> tranvest(Node node ,ArrayList<Integer> res, ArrayList<Integer> visited ) {

        if (visited.contains(node.data)){
            if (!res.contains(node.data)){
                System.out.println("Add data " + node.data);
                res.add(node.data);
            }
        } else {
            visited.add(node.data);
        }

        // go left first
        if (node.left != null && !(visited.contains(node.left.data)) ){
            // to make left and right first that get add for what ever reason
            if (node.root == null) {
                res.add(node.left.data);
            }

            return tranvest(node.left, res, visited);
        }
        else if (node.right != null && !(visited.contains(node.right.data))) {
            // to make left and right first that get add for what ever reason
            if (node.root == null) {
                res.add(node.right.data);
            }

            return tranvest(node.right, res, visited);
        }
        // is leaf than save
        else if (node.left == null && node.left == null) {
            System.out.println("added a leaf with " + node.data);
            res.add(node.data);
            return tranvest(node.root,res,visited);
        } else if (node.root != null) {
            return tranvest(node.root,res,visited);
        }

        // has no root => is root and no more to visted => finish
        else if (node.root == null ) {
            return res;
        } else {
            throw new IllegalStateException("Error while travistating");
        }

    }


    static int[][] swapNodes(int[][] indexes, int[] queries) {

        Node root = new Node(1);
        root.root = null;
        root.add(indexes[0][0],indexes[0][1]);

        create_tree(root.left,indexes,1);
        create_tree(root.right, indexes,2);

        //System.out.println(root.toString());

        // to make left and right first that get add for what ever reason


        ArrayList<Integer> result = tranvest(root,new ArrayList<Integer>(),new ArrayList<Integer>());
        int [] erg = new int[result.size()];

        for (int i = 0; i<result.size(); i++ ) { erg[i] = result.get(i);
        }
        return new int [][]{erg};
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int n = Integer.parseInt(scanner.nextLine().trim());

        int[][] indexes = new int[n][2];

        for (int indexesRowItr = 0; indexesRowItr < n; indexesRowItr++) {
            String[] indexesRowItems = scanner.nextLine().split(" ");

            for (int indexesColumnItr = 0; indexesColumnItr < 2; indexesColumnItr++) {
                int indexesItem = Integer.parseInt(indexesRowItems[indexesColumnItr].trim());
                indexes[indexesRowItr][indexesColumnItr] = indexesItem;
            }
        }

        int queriesCount = Integer.parseInt(scanner.nextLine().trim());

        int[] queries = new int[queriesCount];

        for (int queriesItr = 0; queriesItr < queriesCount; queriesItr++) {
            int queriesItem = Integer.parseInt(scanner.nextLine().trim());
            queries[queriesItr] = queriesItem;
        }

        int[][] result = swapNodes(indexes, queries);

        for (int resultRowItr = 0; resultRowItr < result.length; resultRowItr++) {
            for (int resultColumnItr = 0; resultColumnItr < result[resultRowItr].length; resultColumnItr++) {
                bufferedWriter.write(String.valueOf(result[resultRowItr][resultColumnItr]));

                if (resultColumnItr != result[resultRowItr].length - 1) {
                    bufferedWriter.write(" ");
                }
            }

            if (resultRowItr != result.length - 1) {
                bufferedWriter.write("\n");
            }
        }

        bufferedWriter.newLine();

        bufferedWriter.close();
    }
}