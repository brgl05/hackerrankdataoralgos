package DataStrucktur.Trees.BinarySearchTree.IsThisBinarySearchTree;

public class Solution {


    /* Hidden stub code will pass a root argument to the function below. Complete the function to solve the challenge. Hint: you may want to write one or more helper functions.

    The Node class is defined as follows:
        class Node {
        int data;
        Node left;
        Node right;
         }
    */
    class Node {
        int data;
        Node left;
        Node right;
    }
/* Hidden stub code will pass a root argument to the function below. Complete the function to solve the challenge. Hint: you may want to write one or more helper functions.

The Node class is defined as follows:
    class Node {
    int data;
    Node left;
    Node right;
     }
*/




    boolean checkBST(Node root) {



        if (root.data > root.left.data && root.data < root.right.data) {

            // linkrTeil des Baums  alle rechten Zweige < root
            return leftSide(root.left,root.data,0)
                    //  rechter alle rechten > root
                    && rightSide(root.right,Integer.MAX_VALUE,root.data);

        } else {
            return false;
        }
    }


    private boolean leftSide(Node root, int maxForRight, int minForLeft) {
        // 4 fälle
        // 1 Fall es geht bis ende alles durch  also Super
        // TAktik 1 Fehler macht alles zunichte wenn der nicht auftaucht dann true

        if (root.left == null && root.right == null) {
            return true;
        }
        // erstmal links und rechts dann kann ich mir bei wenn es 2 Kinder gibt die Zeile == null             sparren
        else if (root.right == null && root.left.data > minForLeft && root.left.data <                  root.data) {
            return  leftSide(root.left, maxForRight, root.left.data);
        }
        // rechts starten wenn left null größer als sein Parent aber kleiner als Starter
        else if (root.left == null && root.right.data > root.data
                && root.right.data < maxForRight) {
            return   leftSide(root.right, root.right.data, minForLeft);
        }

        // 2 kindaah da  ach du heidenei
        // erstmal left abfangen
        else if (root.left.data < root.data && root.left.data > minForLeft/**jetzt kommt rechts*/ &&
                root.right.data> root.data && root.right.data < maxForRight) {

            // siehe Notizzettel erst links
            return leftSide(root.left,root.right.data,minForLeft) &&
                    leftSide(root.right,maxForRight,root.left.data);
            // root.left.data als min for left hier liegt der Hund begraben siehe notizZettel
        } else {
            return false;
        }


    }






    // same fun but reverse weil logischerweise alle Linken im rechten teil > Mainroot
// Mainroot der erste der ersten quasi der tom bombadil des Baumes
    private boolean rightSide(Node root, int maxForRight, int minForLeft) {

        if (root.left == null && root.right == null) {
            return true;
        }

        else if (root.right == null && root.left.data > minForLeft && root.left.data < root.data) {
            return rightSide(root.left,root.left.data,minForLeft);
        }
        else if (root.left==null && root.right.data < maxForRight && root.right.data > root.data) {
            return      rightSide(root.right,maxForRight,root.right.data);

        }   else if (root.right.data < maxForRight && root.right.data > root.data  &&
                root.left.data > minForLeft && root.left.data < root.data)
        {
            return rightSide(root.left,root.right.data,minForLeft) &&
                    rightSide(root.right,maxForRight,root.data);

        }       else {
            return false;
        }

    }   }