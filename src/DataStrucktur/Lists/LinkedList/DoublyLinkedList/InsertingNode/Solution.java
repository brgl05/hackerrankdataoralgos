package DataStrucktur.Lists.LinkedList.DoublyLinkedList.InsertingNode;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    static class DoublyLinkedListNode {
        public int data;
        public DoublyLinkedListNode next;
        public DoublyLinkedListNode prev;

        public DoublyLinkedListNode(int nodeData) {
            this.data = nodeData;
            this.next = null;
            this.prev = null;
        }
    }

    static class DoublyLinkedList {
        public DoublyLinkedListNode head;
        public DoublyLinkedListNode tail;

        public DoublyLinkedList() {
            this.head = null;
            this.tail = null;
        }

        public void insertNode(int nodeData) {
            DoublyLinkedListNode node = new DoublyLinkedListNode(nodeData);

            if (this.head == null) {
                this.head = node;
            } else {
                this.tail.next = node;
                node.prev = this.tail;
            }

            this.tail = node;
        }
    }

    public static void printDoublyLinkedList(DoublyLinkedListNode node, String sep, BufferedWriter bufferedWriter) throws IOException {
        while (node != null) {
            bufferedWriter.write(String.valueOf(node.data));

            node = node.next;

            if (node != null) {
                bufferedWriter.write(sep);
            }
        }
    }

    // ist das head nicht auf dann quasi das letzte element

    static DoublyLinkedListNode sortedInsert(DoublyLinkedListNode head, int data) {

        // the newDataNode to insert
        DoublyLinkedListNode newNode = new DoublyLinkedListNode(data);

        // for the Case Data is smaller than the hole List
        if (newNode.data < head.data) {
            newNode.next = head;
            head.prev = newNode;
            return newNode;
        }
        // Coby of head to checking the List
        DoublyLinkedListNode fakeHead = head;


        while (fakeHead.next != null) {

            if (newNode.data >= fakeHead.data && newNode.data < fakeHead.next.data) {

                // new Node in·i·ti·a·li·sie·ren
                newNode.next = fakeHead.next;
                newNode.prev = fakeHead;

                // setting the Linking of the next and preNode on data
                fakeHead.next = newNode;
                fakeHead.next.prev = newNode;
                return head;
            }

            fakeHead = fakeHead.next;
        }

        // in the Case of noData inside the DLinkedList is bigger than data and head.next is now null
        fakeHead.next = newNode;
        newNode.prev = head;

        return head;
    }

    }
