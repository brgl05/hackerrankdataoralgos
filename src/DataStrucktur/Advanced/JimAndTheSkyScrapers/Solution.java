import java.io.*;
import java.math.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

public class Solution {

    // Complete the solve function below.
    
    static long solve(int[] arr){
        // Idee mit Queue
        
        // einmal durchgehen array -> 
        // wenn input kleiner  -> Input in Stack
        // wenn input == -> Input 
        // wenn Input > -> pop Value -> bis input >= 
                // wenn -> output stack bzw 3 * (selbe zahl) -> 2hoch3 zu counter
        long counter = 0;        
        Stack<Integer> stack = new Stack<>();        
                
        for (int i = 0; i<arr.length; i++) {
            if (stack.empty()) {
                stack.add(arr[i]);
            } else {
                if (stack.peek()>=arr[i]) {
                    stack.add(arr[i]);
                } else {
                    while(stack.peek() < arr[i] && !stack.empty()) {
                        counter += get_multiplier(stack,stack.pop());
                        if (stack.empty()) {
                            break;
                        }
                    }
                     stack.add(arr[i]);
                }
            }   
        }
        
        while(!stack.empty()) {
            counter += get_multiplier(stack,stack.pop()); 
        }
        
        System.out.print(counter * 2);
        return counter * 2;          
    }
    
    static long get_multiplier(Stack<Integer> stack, int value) {
        
        long counter = 0;
        while (!stack.empty() && stack.peek() == value ) {
            stack.pop();
            ++counter;
        }
        
        // gausische Summenformel für formel 5 + 4+ 3+ 2 -> n*n+1/2 
        return counter*(counter+1)/2;
    }
    
    
    static long slow_solve(int[] arr) {
        int counter = 0;
        
        for (int i = 0; i< arr.length - 1; i++ ) {
            for (int j = i+1 ; j < arr.length; j++) {
                if (arr[i] == arr[j]) {
                    counter++;
                    counter++;
                } else if (arr[i] >= arr[j]) {
                    continue;
                } else {
                    break;
                }
            }
        }
            return counter;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int arrCount = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] arr = new int[arrCount];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int arrItr = 0; arrItr < arrCount; arrItr++) {
            int arrItem = Integer.parseInt(arrItems[arrItr]);
            arr[arrItr] = arrItem;
        }

        long result = solve(arr);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
