import java.math.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

public class Solution {

    /*
     * Complete the waiter function below.
     */
     
    static boolean isPrime(int nr) {
        for (int i = nr - 1 ; i>1; i--) {
                if (nr % i == 0){
                   return false;
                }
            }
            return true;
    }
     
    static int[]  returnPrimes(int size) {
        int [] result = new int[size];
        int counter = 0;
       
        
        for (int nr = 2 ; nr< Integer.MAX_VALUE; nr++){
            if (counter == size ){
                System.out.println(Arrays.toString(result));
                return result;
            }    
            if (isPrime(nr) ) {
            result[counter++] = nr;
            }
        }
        System.out.println(Arrays.toString(result));
        return result;
    }
     
     
    static int[] waiter(int[] number, int q) {
        
        Stack<Integer> a = new Stack();
        
        for (int nr : number){
            a.push(nr);
        }
        
        int [] primes = returnPrimes(q);
        
        int [] answer = new int[10000]; 
        Stack<Integer> b_Prime = new Stack();
        Stack<Integer> a_NoPrime = new Stack();
        
         int index_res = 0; 
         for (int i = 0; i< q;i++) {
            
            if (!a_NoPrime.empty()){
                    a = (Stack<Integer>) a_NoPrime.clone(); 
                    a_NoPrime.clear(); 
            }
            while (!a.empty()) {
                if ((a.peek())%primes[i] == 0) {
                    System.out.println("push to b_Prime " + a.peek());
                    b_Prime.push(a.pop());
                }  else   {
                    System.out.println("push to a_Prime " + a.peek());
                    a_NoPrime.add(a.pop());
                }
            }           
            while(!b_Prime.isEmpty()) {
                System.out.println("push to result" +b_Prime.peek());
                answer[index_res++] = b_Prime.pop();
            }
        
         }
       
        
         while(!a_NoPrime.isEmpty()) {
             System.out.println("!");
            answer[index_res++] = a_NoPrime.pop();
        }
        
       int [] anwersmall = Arrays.copyOfRange(answer,0,index_res);
        
        return anwersmall;
        

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String[] nq = scanner.nextLine().split(" ");

        int n = Integer.parseInt(nq[0].trim());

        int q = Integer.parseInt(nq[1].trim());

        int[] number = new int[n];

        String[] numberItems = scanner.nextLine().split(" ");

        for (int numberItr = 0; numberItr < n; numberItr++) {
            int numberItem = Integer.parseInt(numberItems[numberItr].trim());
            number[numberItr] = numberItem;
        }

        int[] result = waiter(number, q);

        for (int resultItr = 0; resultItr < result.length; resultItr++) {
            bufferedWriter.write(String.valueOf(result[resultItr]));

            if (resultItr != result.length - 1) {
                bufferedWriter.write("\n");
            }
        }

        bufferedWriter.newLine();

        bufferedWriter.close();
    }
}
