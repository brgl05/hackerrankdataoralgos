#!/bin/python3

import math
import os
import random
import re
import sys


def get_match(br):
    if br == ")":  return "("
    if br == "]": return "["
    if br == "}": return "{"
    return "";

    # )


def findMatch(br, array):
    for i in range(len(array) - 1, 0, -1):
        # print("i = " + str(i) + "ar = " + array[i])
        if (get_match(br) == array[i]):
            return array[0:i]
        else:
            return 'wrong'
    return ""


# Complete the isBalanced function below
def isBalanced(s):
    #print(s)

    if (len(s) == 0):
        return 'YES'

    if (len(s) % 2 != 0):
        return 'NO'

    if ('wrong' in s):
        return 'NO'

    if (s == ''):
        return 'YES'

    for i in range(0, len(s)):
        if s[i] in ["}", "]", ")"]:
            # print("found :" + s[i])
            return isBalanced((findMatch(s[i], s[0:i])) + (s[i + 1:len(s)]))

    return 'NO'


if __name__ == '__main__':
    st = "{()[[[]]({{}[{}]}){((()))([[]]([]{}[]{{()({})[{(())[]}]([{[]}])}()[][[]]()[([{{{(){[[(()(([()]([][]()()){[{}][((()[()])[{(([([]{({})})](){[]{}[[[][{{{}{[()]{[]{}}}(())}}]()[{}{}{[[]][[]{{()}}]}{([[[]]({[]})]{}){}}][[{[()]}]][]{}]{}()]{}})([])){}}[{}[]]{}]){[{}]{{{}[()](){}}[[{}]]{[{}]}{}()([]){}[][]}([]((){[][]{[({{}}[[[][()]][[][]]{}]){[][[]()]{}}[]]({}[{(([[]()](){((({()(){([])()[{{[[{}][]]}(){}[][][[]{}[]{{[(([{}]))]()[]}(())}][([])[{(())}](())[]([[([])]])]}]}}({}{}))))}){{}}(({()}){})[](([]()()[{([]){}(){(({}[]))}}()](()){{}{}}){({})}[[([[]()][])()[]]()])(()))}])()}{({()})(([[]]{()}[]))}})()(){[]})()[{{{}}[]}{}{}[][(({}[[{}(){}]]){{[[]{[{(()[])((([[]])((())))){}[][]}]({})}(())]}{}((){{}((){[[][]]}([]){({}[[]({}{}){}])}(){})}(()))[]}({([][])})()){}]]}{(){}}()]{[]}{}[]{}{{}{}}[{}{{()}}]({(){[]}({}[]()[])({}{{{[]{{}}}{}[][{}[]][][]}[{{()}()[{}[]]{}([]())}]})[]({[]})})[]}{(())[[]]})[](){[[]{}]{}({{}(){[]}}[]{{}[[][]][]()}){{}}})[(()[])[()[[{}()]()]()[]]])][[]({([{{}}])})]]}}}}])]}))}]}"
    result = []
    fp = open("input.txt", "r")
    count = int(fp.readline())
    inputs = []
   # for i in range(0,count):
    #    inputs.append(str(fp.readline()));
    inputs = fp.read().split('\n')

    fp.close()
    f = open("my_result.txt", "a");

    print(st)
    print(inputs[1])
    print(st == inputs[1])
    print(str(len(st)) + "!=" + str(len(inputs[1])) )
    print(len(st) == len(inputs[1]))

    for i in range(0,len(st)):
        if st[i] != inputs[1][i]:
            print(st[i] + "!=" + inputs[1][i] +"\t")


    for i in range(0,count):

        if st == inputs[i]:
            print("111111111111")
        result ='';
        result = isBalanced(inputs[i])
        #print(result)
        f.write(result + '\n')

    f.close()