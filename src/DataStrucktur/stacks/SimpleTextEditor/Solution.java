import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {


    
    
    public static void main(String[] args) throws IOException  {
        BufferedReader bi = new BufferedReader(new InputStreamReader(System.in));
String line;

       
        Stack<String> stack = new Stack();
        String res = "";
        
        while ((line = bi.readLine()) != null)  {
            String [] nk = line.split(" ");
           
            if (nk.length == 1) {
                if (stack.empty()) {
                    continue;
                } else {
                    stack.pop();
                    if (stack.empty()) {
                        res = "";
                    }  else {
                    res = stack.peek();
                    }
                }   
                
            } else if (Integer.valueOf(nk[0]) == 1) {
                
                if (stack.empty()) {
                    stack.push(nk[1]);
                    res = nk[1];
                } else {
                    res = res + nk[1];
                    stack.push(res);
                }
                
            } else if (Integer.valueOf(nk[0]) == 2) {
                res = res.substring(0,(res.length() - Integer.valueOf(nk[1])));
                stack.push(res);
                
            } else if (Integer.valueOf(nk[0]) == 3) {
                if (res.length() < Integer.valueOf(nk[1])) {
                    continue;
                }
                System.out.println(res.charAt(Integer.valueOf(nk[1])- 1));
                
            } else {
                System.out.println("Error dont know " + nk[0]);
            }
            
            //System.out.println("state of res = " +  res);
               
        }
        
    }
}
