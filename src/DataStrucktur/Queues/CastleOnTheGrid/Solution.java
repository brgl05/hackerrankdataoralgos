package DataStrucktur.Queues.CastleOnTheGrid;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;



public class Solution {

    static int lengthGrid = 0;

    static HashMap< Point, Integer> visited = new HashMap<Point, Integer>();

    static  void getNeighbours(Point point, char [] [] grid, Point goal,  Queue<Point> queue ) {
        ArrayList<Integer> res = new ArrayList<>();
        int posX = point.pointX;
        int posY = point.pointY;
        int currentPathLength = visited.get(point);

        // Find every Possible Combination on the X axis
        int tmpX = point.pointX + 1;
        while (tmpX < lengthGrid && grid[tmpX][posY] == '.') {
            Point tmp = new Point(tmpX++,posY);
            // already in visited
            if (visited.containsKey(tmp)) {
                // schauen ob es nun ein neuen schnellern weg gibt
                if (visited.get(tmp) > currentPathLength + 1 ) {
                    visited.replace(tmp,currentPathLength+1);
                    queue.add(tmp);
                } else {
                    if (tmp == goal) {
                        return;
                    }

                }
            } else {
                // neuer Nachbar gefunden
                // kommt in die Queue
                visited.put(tmp,currentPathLength + 1);
                queue.add(tmp);
            }
            // wenn Goal gefunden direkt abbrechen
            if (tmp == goal) {
                return;
            }

        }

        tmpX = point.pointX - 1;
        while (tmpX >= 0 && grid[tmpX][posY] == '.') {
            Point tmp = new Point(tmpX--,posY);
            // already in visited
            if (visited.containsKey(tmp)) {
                // schauen ob es nun ein neuen schnellern weg gibt
                if (visited.get(tmp) > currentPathLength + 1 ) {
                    visited.replace(tmp,currentPathLength+1);
                    queue.add(tmp);
                } else {
                    if (tmp == goal) {
                        return;
                    }

                }
            } else {
                // neuer Nachbar gefunden
                // kommt in die Queue
                visited.put(tmp,currentPathLength + 1);
                queue.add(tmp);
            }
            // wenn Goal gefunden direkt abbrechen
            if (tmp == goal) {
                return;
            }

        }

        int tmpY = point.pointY + 1;
        while (tmpY < lengthGrid && grid[posX][tmpY] == '.') {
            Point tmp = new Point(posX,tmpY++);
            // already in visited
            if (visited.containsKey(tmp)) {
                // schauen ob es nun ein neuen schnellern weg gibt
                if (visited.get(tmp) > currentPathLength + 1 ) {
                    visited.replace(tmp,currentPathLength+1);
                    queue.add(tmp);
                } else {
                    if (tmp == goal) {
                        return;
                    }

                }
            } else {
                // neuer Nachbar gefunden
                // kommt in die Queue
                visited.put(tmp,currentPathLength + 1);
                queue.add(tmp);
            }
            // wenn Goal gefunden direkt abbrechen
            if (tmp == goal) {
                return;
            }

        }
        tmpY = point.pointY - 1;
        while (tmpY >= 0 && grid[posX][tmpY] == '.') {
            Point tmp = new Point(posX,tmpY--);
            // already in visited
            if (visited.containsKey(tmp)) {
                // schauen ob es nun ein neuen schnellern weg gibt
                if (visited.get(tmp) > currentPathLength + 1 ) {
                    visited.replace(tmp,currentPathLength+1);
                    queue.add(tmp);
                } else {
                    if (tmp == goal) {
                        return;
                    }

                }
            } else {
                // neuer Nachbar gefunden
                // kommt in die Queue
                visited.put(tmp,currentPathLength + 1);
                queue.add(tmp);
            }
            // wenn Goal gefunden direkt abbrechen
            if (tmp == goal) {
                return;
            }

        }

    }



    // Complete the minimumMoves function below.
    static int minimumMoves(String[] grid, int startX, int startY, int goalX, int goalY)    {

        Point goal = new Point(goalX,goalY);
        int result = Integer.MAX_VALUE;
        lengthGrid = grid[0].length();
        char [] [] gridArr = createGrid(grid);

        Queue<Point> queue = new LinkedList<>();
        Point startPoint = new Point(startX,startY);
        visited.put(startPoint,0);
        queue.add(startPoint);


        while(!queue.isEmpty()){
            getNeighbours(queue.poll(),gridArr,goal,queue);
        }

        return visited.get(goal);
    }

    static char [] [] createGrid(String [] grid){

        char [] [] gridArr = new char[grid.length][lengthGrid];
        for (int i = 0; i<grid.length; i++) {
            char [] tmp = grid[i].toCharArray();
            for (int j = 0;j<lengthGrid; j++){
                gridArr[i][j] = tmp[j];
            }
        }
        return gridArr;
    }

    static class Point {

        public Point(int pointX, int pointY) {
            this.pointX = pointX;
            this.pointY = pointY;
        }

        public int pointX;
        public int pointY;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Point point = (Point) o;
            return pointX == point.pointX &&
                    pointY == point.pointY;
        }

        @Override
        public int hashCode() {
            return Objects.hash(pointX, pointY);
        }

        @Override
        public String toString() {

            return "";
        }
    }




    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        String[] grid = new String[n];

        for (int i = 0; i < n; i++) {
            String gridItem = scanner.nextLine();
            grid[i] = gridItem;
        }

        String[] startXStartY = scanner.nextLine().split(" ");

        int startX = Integer.parseInt(startXStartY[0]);

        int startY = Integer.parseInt(startXStartY[1]);

        int goalX = Integer.parseInt(startXStartY[2]);

        int goalY = Integer.parseInt(startXStartY[3]);

        int result = minimumMoves(grid, startX, startY, goalX, goalY);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
