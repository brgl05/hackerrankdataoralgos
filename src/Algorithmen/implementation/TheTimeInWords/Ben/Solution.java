package Algorithmen.implementation.TheTimeInWords.Ben;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Solution {

    // Complete the timeInWords function below.
    static String timeInWords(int h, int m) {


        if (m == 0 ) {
            return giveHour(h) +  " o' clock";
        } else if ( m <= 30) {
            return  giveMinutes(m) + " past " + giveHour(h);
        } else {
            return giveMinutes (60 - m)  +    " to " +  giveHour(h +1);
        }

    }

    //three minutes to eleven
    static String giveHour(int h) {
        String hour;
        switch(h) {
            case 1: hour = "one";break;
            case 2: hour = "two";break;
            case 3: hour = "three"; break;
            case 4: hour = "four"; break;
            case 5: hour = "five "; break;
            case 6: hour = "six"; break;
            case 7: hour = "seven"; break;
            case 8: hour = "eight"; break;
            case 9: hour = "nine"; break;
            case 10: hour = "ten"; break;
            case 12: hour = "twelve"; break;
            default : hour = "eleven";
        }
        return hour;
    }


    // three minutes to eleven
//three minutes to eleven
//twenty five minutes to seven
    static   String giveMinutes(int mint) {

        int einz = mint % 10;
        int zwei = mint / 10;

        System.out.println(einz);
        System.out.println(zwei);

        if (zwei == 1) {

            switch (einz) {
                case 0: return "ten minutes" ;
                case 1: return "eleven minutes";
                case 2: return "twelve minutes";
                case 3: return "thirteen minutes";
                case 4: return "fourteen minutes";
                case 5: return "quarter";
                case 6: return "sixteen minutes";
                case 7: return "seventeen minutes";
                case 8: return "eightteen minutes";
                case 9: return "nineteen minutes";
            }
        }

        if (mint == 30) {  return "half";}
        if (mint == 45) { return "quarter";}
        if (mint == 1)   { return "one minute";}


        if (mint < 10) {
            return giveSzuZahl(einz) +  " minutes";
        }

        return "" + hoursToString(zwei) + " " + giveSzuZahl(einz) +  " minutes";
    }




    static String hoursToString(int h) {


        switch (h) {
            case 0 : return "";

            case 2 : return "twenty";
            case 3 : return "thirty";
            case 4 : return "forty";
            case 5 : return "fifty";
        }

        return "Fehler in Hours to String";
    }


    static String giveSzuZahl(int m) {

        String hour = "";
        switch(m) {


            case 1: hour = "one";break;
            case 2: hour = "two";break;
            case 3: hour = "three"; break;
            case 4: hour = "four"; break;
            case 5: hour = "five"; break;
            case 6: hour = "six"; break;
            case 7: hour = "seven"; break;
            case 8: hour = "eight"; break;
            case 9: hour = "nine"; break;
        }
        return hour;
    }





    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int h = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int m = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        String result = timeInWords(h, m);

        bufferedWriter.write(result);
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
