package Algorithmen.Sonstige.SurfaceArea3D;

import java.io.*;

import java.util.*;


public class Solution {



    static int surfaceArea(int[][] A) {

        int res = 0;
        int sufField = 0; // this is only one surface from the Field bottom to top
        int toug = 0; // surfaces whitch toughing each other


        for (int col = 0; col < A.length; col++) {

            for (int rows = 0; rows < A[col].length; rows++) {

                toug = 0;

                // this formular is because if there are some quarters on each other the top of one quarter is on the top of on bottom of  another

                sufField = 6*  A[col][rows]  -   (A[col][rows] -2 + A[col][rows]);

                // check the 4 Directions x+1 x -1 y+1 y-1

                if (col - 1 >= 0) {
                    if (A[col][rows] > A[col - 1][rows]) {
                        toug += A[col - 1][rows];
                    } else {
                        toug += A[col][rows];
                    }
                }
                if (rows - 1 >= 0) {
                    if (A[col][rows] > A[col][rows - 1]) {
                        toug += A[col][rows - 1];
                    } else {
                        toug += A[col][rows];
                    }
                }


                if (col + 1 <= A.length -1 ) {
                    if (A[col][rows] > A[col+1][rows]) {
                        toug += A[col+1][rows ];
                    } else {
                        toug += A[col][rows];
                    }
                }
                if (rows + 1 <=  A[col].length - 1 ) {
                    if (A[col][rows] > A[col][rows+1]) {
                        toug += A[col][rows +1];
                    } else {
                        toug += A[col][rows];
                    }
                }

                System.out.println("sufField for col = " + col + " row =" + rows + " = " + sufField + "  berührungen = " );
                System.out.println(toug);

                res += sufField - toug;
            }


        }
        return res;

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String[] HW = scanner.nextLine().split(" ");

        int H = Integer.parseInt(HW[0]);

        int W = Integer.parseInt(HW[1]);

        int[][] A = new int[H][W];

        for (int i = 0; i < H; i++) {
            String[] ARowItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int j = 0; j < W; j++) {
                int AItem = Integer.parseInt(ARowItems[j]);
                A[i][j] = AItem;
            }
        }

        int result = surfaceArea(A);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}