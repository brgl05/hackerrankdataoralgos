package Algorithmen.encryption;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        System.out.println(encryption("feedthedog"));

    }

    // Complete the encryption function below.
    static String encryption(String s) {


        double wurzel = Math.sqrt(s.length());


        int col = (int) Math.ceil(wurzel);
        int rows = (int) wurzel;

        if (rows * col < s.length()) {
            rows += 1;
        }

        char[] txt = s.toCharArray();


        StringBuilder sB = new StringBuilder();
        for (int i = 0; i < col; i++) {

            for (int j = 0; j < rows; j++) {
                if ((i + col * j) < s.length()) {
                    if (j == 0) {
                        sB.append(txt[i]);

                    } else {
                        sB.append(txt[i + col * j]);
                    }
                }
            }

            sB.append(" ");

        }


        return String.valueOf(sB);
    }
}
