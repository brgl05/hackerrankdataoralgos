package Algorithmen.Dynamic_Programming.MaxSumForNonAdjacentElements.mySolution;


import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;


// not really Dynamic but time Complexipiliy is good 0(n) whitch is good
    public class Solution {

        // Complete the maxSubsetSum function below.
        static int maxSubsetSum(int[] arr) {

            if (arr.length == 0) {
                return   Math.max(0, arr[0]);
            }
            else if (arr.length == 1  ) {
                return Math.max(arr[0],arr[1]);
            }
            else  {


                return maxSubsetSum(arr,0,0,0);
            }
        }

        static int maxSubsetSum(int[] arr,int index,int  exc,  int inc) {
            int newExclusive = inc;
            inc = Math.max(inc,exc +arr[index]);

            if (index < arr.length -1 ) {
                return maxSubsetSum (arr,index +1,newExclusive,inc);
            }
            return inc;
        }


        private static final Scanner scanner = new Scanner(System.in);

        public static void main(String[] args) throws IOException {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

            int n = scanner.nextInt();
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            int[] arr = new int[n];

            String[] arrItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int i = 0; i < n; i++) {
                int arrItem = Integer.parseInt(arrItems[i]);
                arr[i] = arrItem;
            }

            int res = maxSubsetSum(arr);

            bufferedWriter.write(String.valueOf(res));
            bufferedWriter.newLine();

            bufferedWriter.close();

            scanner.close();
        }
    }

