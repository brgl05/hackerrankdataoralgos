package Algorithmen.Greedy.Greedy_Florist;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the getMinimumCost function below.
    static int getMinimumCost(int k, int[] cost) {

        Arrays.sort(cost);
        int gesamtkosten = 0;


        int faktor = 0; // die wievielte Blume gekauft von Person k gekauft wird wird
        int käufer = k;

        int i = cost.length-1;
        for (int j = k; i>=0;j--) {
            System.out.println("i = " + i + " j ist gleich " + j );
            gesamtkosten += (faktor + 1 ) * cost[i];
            i = i -1;
            if (j==1) {
                faktor +=1;
                j= k+1; // kein plan wieso beim ersten Schleifen durchlauf k -1 ???
            }
        }

        return gesamtkosten;
    }





    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String[] nk = scanner.nextLine().split(" ");

        int n = Integer.parseInt(nk[0]);

        int k = Integer.parseInt(nk[1]);

        int[] c = new int[n];

        String[] cItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int cItem = Integer.parseInt(cItems[i]);
            c[i] = cItem;
        }

        int minimumCost = getMinimumCost(k, c);

        bufferedWriter.write(String.valueOf(minimumCost));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
