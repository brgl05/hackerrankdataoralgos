package Algorithmen.QueenAttak2;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

// eventuell fehler wenn 2 nebeneinanderstehen

// Complete the queensAttack function below.

    static int queensAttack(int n, int k, int r_q, int c_q, int[][] obstacles) {


        int topRight = n -  Math.max(r_q,c_q);
        int bottumLeft = Math.min(r_q,c_q) - 1;

        // wie nah col an null oder row an n
        int topLeft = Math.min(c_q - 1,n - r_q);
        // wie nah row an null
        int bottumRight = Math.min(r_q-1,n-c_q);


        // In UrzeigerSinn alle Geraden linien mit checken
        // erstmal als wäre es ein Turm behandeln
        int top = n - r_q;
        int rechts = n - c_q;
        int bottom = r_q - 1;
        int links = c_q - 1;


        for (int i = 0; i < k; i++) {

            // daten der hindernisse
            int rows = obstacles[i][0];
            int col = obstacles[i][1];

            // selbe Reihe
            if (rows == r_q) {
                // hindernis rechts von Queen
                if (col > c_q) {
                    if ( rechts > (col - (c_q + 1)))
                        rechts = col - (c_q + 1); // +1 weil Feld wo hinderniss
                } else // Links von Queen Hinderniss
                {           if (links > c_q - (col + 1))
                    links = c_q - (col + 1) ;
                }
                // selbe Spalte beinflusst also top bottom
            } else if (col == c_q) {
                // ist er überhalb
                if (rows > r_q) {

                    if  (top > rows -  (r_q + 1))
                        top = rows -  (r_q + 1) ;
                } else {

                    if (bottom > r_q - (rows + 1))
                        bottom = r_q - (rows + 1);
                }

                // Unten Links oben rechts line
            } else if (rows - r_q == col - c_q ) {

                if (rows > r_q) {

                    if (topRight > rows - (r_q + 1))
                        topRight = rows - (r_q + 1);

                } else {
                    if ( bottumLeft > r_q - (rows +1 ))
                        bottumLeft = r_q - (rows +1 );
                }
            } else if (Math.abs(rows - r_q) == Math.abs(col - c_q)) {
                if (rows > r_q) {

                    if (topLeft >  rows - (r_q + 1))
                        topLeft =  rows - (r_q + 1);
                }
                else {
                    if ( bottumRight > r_q - (rows +1 ))
                        bottumRight = r_q - (rows +1 );
                }
            } else {
                System.out.println("rows" + rows + " col " + col);
                continue;
            }


        }


        int senkr = top + links + rechts + bottom;

        int quer = topLeft + topRight + bottumLeft + bottumRight;
        System.out.println("Quere " + quer);
        return senkr + quer ;

    }





    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String[] nk = scanner.nextLine().split(" ");

        int n = Integer.parseInt(nk[0]);

        int k = Integer.parseInt(nk[1]);

        String[] r_qC_q = scanner.nextLine().split(" ");

        int r_q = Integer.parseInt(r_qC_q[0]);

        int c_q = Integer.parseInt(r_qC_q[1]);

        int[][] obstacles = new int[k][2];

        for (int i = 0; i < k; i++) {
            String[] obstaclesRowItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int j = 0; j < 2; j++) {
                int obstaclesItem = Integer.parseInt(obstaclesRowItems[j]);
                obstacles[i][j] = obstaclesItem;
            }
        }

        int result = queensAttack(n, k, r_q, c_q, obstacles);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}