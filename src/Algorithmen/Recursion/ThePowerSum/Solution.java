package Algorithmen.Recursion.ThePowerSum;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the powerSum function below.
    static int powerSum(int X, int N) {
        int erg = 0;

        double x = (double) X;
        double n = (double) N;

        double wurzelFaktorN = Math.pow(x,1/n);

            if ( wurzelFaktorN == Math.floor(wurzelFaktorN)) {
                System.out.println("gerade wurzel zahl");
                erg += 1;
        } else  {
                System.out.println("ungerade Wurzelzahl");
        }



   erg +=  powerSumRecursion(X,N,((int) Math.floor(wurzelFaktorN) -1 ));
        return erg;
    }

    static int powerSumRecursion (int X,int N, int maxFaktor) {



        System.out.println("ich gege rein mit den werten  X = " + X + " N " + N + " maxfaktor " + maxFaktor);
        // wenn alles aufgeht dann 1 drauf
       if (X - Math.pow(maxFaktor,N)== 0) {
           System.out.println("ich returne 1 wegen Faktor " + maxFaktor);
           return 1;
       }

        int XminusFakor = X - (int) Math.pow(maxFaktor,N);
       // wenn kein prime dann vorbei
       if(!isPrime(XminusFakor)) {
           powerSumRecursion(XminusFakor,N,maxFaktor-1);
       }
       else if (maxFaktor >1) {
           powerSumRecursion(X,N,maxFaktor -1);
       }
        return 0;
    }


    static boolean  isPrime(int n) {
        //check if n is a multiple of 2
        if (n%2==0) return false;

        for(int i=3;i*i<=n;i+=2) {
            if(n%i==0)
                return false;
        }
        return true;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {




        System.out.println(powerSum(100,2));

        /**
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int X = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int N = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int result = powerSum(X, N);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    */
    }
}
